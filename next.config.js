/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  compiler: "es6"
}

module.exports = nextConfig
