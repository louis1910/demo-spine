import '@/styles/globals.css'
import '@esotericsoftware/spine-player/dist/spine-player.css';

export default function App({ Component, pageProps }) {
  return <Component {...pageProps} />
}
