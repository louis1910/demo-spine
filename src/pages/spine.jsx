import React from "react";
import dynamic from "next/dynamic";

const SpinePlayer = dynamic(() => import('@/components/player/Spine'), {
  ssr: false,
})

const SpinePage = () => {
  return <div style={{
    width: 640,
    height: 480,
  }} id="player-container">
    <SpinePlayer />
  </div>
}

export default SpinePage;