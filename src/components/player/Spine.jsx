import React, {lazy, useRef} from 'react';
import { SpinePlayer } from '@esotericsoftware/spine-player';


const Spine = () => {

  // const jsonUrl = 'bg/BG_Tiger_Crystal.json';
  // const atlasUrl = 'bg/BG_Tiger_Crystal.atlas';

  // const jsonUrl = "https://esotericsoftware.com/files/examples/4.0/spineboy/export/spineboy-pro.json";
  // const atlasUrl = "https://esotericsoftware.com/files/examples/4.0/spineboy/export/spineboy-pma.atlas";

  const jsonUrl = "bg/Tiger_Crystal_BG/export/BG_Tiger_Crystal.json";
  const atlasUrl = "bg/Tiger_Crystal_BG/export/BG_Tiger_Crystal.atlas";

  new SpinePlayer('player-container', {
    jsonUrl: jsonUrl,
    atlasUrl: atlasUrl,
    animation: '0_BG_Smoke',
    // animations: ['0_BG_Smoke', '1_BG_Smoke_To_Fire', '2_BG_Fire', '3_BG_Fire_To_More', '4_BG_Fire_More', 'Snow'],
    // showControls: true,
    // backgroundColor: '#ff00ffff',
    // premultipliedAlpha: false,
    // backgroundImage: {
    //   url: 'bg/BG_Tiger_Crystal.png',
    // },
  });

  return <div></div>;
};

export default Spine;


